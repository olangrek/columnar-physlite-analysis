# Columnar PHYSLITE analysis

### PHYSLITE with RDF  or uproot - the basics

ROOT can directly read the xAOD auxiliary store contents without any ATLAS libraries, provided they are expressed as dynamic variables, as is the case in PHYSLITE. An example of such a branch is AnalysisMuonsAuxDyn.pt. This can
be read directly in RDataFrame and in uproot. 

#### RDataFrame
The dataframe displays PHYSLITE in a tabular manner, with one row for each event. Consequently $p_T$ , $\eta$ etc. exist as columns in the dataframe for each object.

The notebook basics/'PHYSLITE with RDF - the basics.ipynb' contains an introduction to analysis with RDF. There are examples of how to explore what branches are available, how to make cuts and how to make histograms with matplotlib.
A PHYSLITE is required to run the notebook. The PHYSLITE used in the example can be found in the Datafiles folder. No other files are required to run the notebook.

